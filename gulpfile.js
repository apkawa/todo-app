'use strict';

var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    path = require('path'),
    uglify = require('gulp-uglify'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer-core'),
    webpack = require('gulp-webpack')

    ;

function swallowError(error) {

    //If you want details of the error in the console
    console.log(error.toString());

    this.emit('end');
}

var app_root = './public/app/';
var dist_root = './public/dist/';
var options = {
    less: app_root + '/css/**/*.less',
    js: app_root + '/js/todo/app.jsx',
    html: app_root + '/*.html',
    dist: dist_root,
    webpack_config: require('./webpack.config.js'),
};

var dist_dirs = {
    js: dist_root + '/js/',
    html: dist_root,
    css: dist_root + '/css/',
};

gulp.task('html', function () {
    gulp.src(options.html)
        .pipe(gulp.dest(dist_dirs.html))
});

gulp.task('less', function () {
    var processors = [
        autoprefixer({browsers: ['last 1 version']}),
    ];

    gulp.src(options.less)
        .on('error', swallowError)
        .pipe(less({
            paths: [
                path.join(__dirname, 'less', 'includes'),
                './node_modules'
            ]
        }))
        .pipe(postcss(processors))
        .pipe(gulp.dest(dist_dirs.css));
});


gulp.task('webpack', function () {
    var config = options.webpack_config;
    return gulp.src(options.js)
        .pipe(webpack(config))
        .pipe(gulp.dest(dist_dirs.js));

});

gulp.task('js', ['webpack']);

gulp.task('default', ['less', 'js', 'html'], function () {
    //console.log(options)
});

gulp.task('production', ['default'], function () {
    gulp.src("./dest/bundle.js")
        .pipe(uglify())
        .pipe(rename('bundle.min.js'))
        .pipe(gulp.dest(options.dist))

});


gulp.task('watch', ['default'], function () {
    gulp.watch(app_root + '/**/*', ['default'])
});


// TODO collect
// https://gist.github.com/mamchenkov/3690981
// find ./app -iname '*.js' -o -iname '*.jsx' | xargs xgettext --language=javascript --from-code=utf-8 --keyword=_gt:1
