'use strict';

var path = require('path');
var webpack = require('webpack');

module.exports = {
    cache: true,
    debug: true,
    devtool: 'source-map',
    entry: {
        app: './public/app/js/todo/app.jsx'
    },
    devServer: {
        hot: true,
        proxy: {
            '*': 'http://localhost:' + (process.env.PORT || 3000)
        }
    },
    output: {
        filename: '[name].js',
        sourceMapFilename: 'source_maps/[file].map'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: [/node_modules/, /public\/components/],
                loaders: ['babel']
            },
            {
                test: /\.css$/,
                loaders: ['style', 'css']
            },
            {
                test: /\.less$/,
                loaders: ["style", "css", "less"]
            }

        ],
        noParse: /\.min\.js/
    },
    resolve: {
        modulesDirectories: ['shared', 'node_modules'],
        extensions: ['', '.js', '.jsx', '.json']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            jQuery: 'jquery'
            , $: 'jquery'
        })
    ]
};
