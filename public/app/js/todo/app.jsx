'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import createHashHistory from 'history/lib/createHashHistory';

import { createStore, combineReducers, applyMiddleware, createDispatcher, createRedux} from 'redux';
import { Provider, connect } from 'react-redux';

import * as reducers                    from './reducers';
import promiseMiddleware    from './middleware/promiseMiddleware';
import routes      from './routes';

const history = createHashHistory();
const reducer = combineReducers(reducers);
const store = applyMiddleware(promiseMiddleware)(createStore)(reducer);


ReactDOM.render(
    <Provider store={store}>
        <Router children={routes} history={history}/>
    </Provider>,

    document.getElementById('content')
);
