'use strict';

import React from 'react';


export default class EditNodeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            active: null
        }
    }

    render() {
        if (this.props.node) {
            return (
                <div>
                    <div>
                        <textarea value={this.props.node.module}/>
                    </div>
                    <button>Save</button>
                </div>

            );

        }
        return false;


    }
}

