'use strict';

import React from 'react';
import cx from 'classnames';

import Tree from 'react-ui-tree';
import {Grid, Row,  Col } from 'react-bootstrap';

import styles from 'react-ui-tree/lib/react-ui-tree.less';

import tree_data from './tree_data';
import theme from './theme.less';

import EditNodeForm from './EditNodeForm';


export default class TreeApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: null,
            tree: tree_data,

        }
    }

    renderNode = (node) => {
        return (
            <span className={cx('node', {
        'is-active': node === this.state.active
        })} onClick={this.onClickNode.bind(null, node)}>
        {node.module}
      </span>
        );
    }

    onClickNode = (node) => {
        this.setState({
            active: node
        });
    }

    render() {
        return (
            <Row>
                <Col md={3}>
                    <Tree
                        paddingLeft={20}              // left padding for children nodes in pixels
                        tree={this.state.tree}        // tree object
                        onChange={this.handleChange}  // onChange(tree) tree object changed
                        isNodeCollapsed={this.isNodeCollapsed}
                        renderNode={this.renderNode}  // renderNode(node) return react element
                    />
                </Col>
                <Col md={4}>
                    <EditNodeForm node={this.state.active}/>
                </Col>
            </Row>
        )

    }
}
