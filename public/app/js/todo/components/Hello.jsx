'use strict';

import React from 'react';

export default class Hello extends React.Component {
    render() {
        var username = this.props.params.who;
        return <p>Hello, {username}</p>;

    }
}

