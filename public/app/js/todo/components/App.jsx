'use strict';

import React, {PropTypes} from 'react';
import {Grid, Row,  Col } from 'react-bootstrap';

import {NavBar } from './';

export default class AppView extends React.Component {
    static propTypes = {
        children: PropTypes.object
    };

    render() {
        return (
            <Grid id="app-view">
                <Row>
                    <Col xs={12}>
                        <h1>Todos</h1>
                        <hr />
                    </Col>
                </Row>
                <Row>
                    <Col id="nav" xs={4}><NavBar /></Col>
                    <Col xs={8}>
                        {this.props.children}
                    </Col>

                </Row>
            </Grid>
        )
    }
}

