'use strict';

import React from 'react';

import {Link} from 'react-router';
import {Nav, NavItem} from 'react-bootstrap';
import {LinkContainer, IndexLinkContainer} from 'react-router-bootstrap';

export default class NavBar extends React.Component {
    render() {
        return (
            <Nav bsStyle="pills" stacked>
                <IndexLinkContainer to="/">
                    <NavItem eventKey={1}>Home</NavItem>
                </IndexLinkContainer>
                <LinkContainer to="/hello/apkawa">
                    <NavItem eventKey={2}>Hello</NavItem>
                </LinkContainer>
                <LinkContainer to="/todo/">
                    <NavItem eventKey={3}>Todo App</NavItem>
                </LinkContainer>
                <LinkContainer to="/tree">
                    <NavItem eventKey={4}>Tree app</NavItem>
                </LinkContainer>
            </Nav>
        );

    }
}

