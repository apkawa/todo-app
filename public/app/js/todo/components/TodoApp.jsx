'use strict';

import React, {PropTypes} from 'react';
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux';

import * as TodoActions       from '../actions/TodoActions';

import TodoView from './TodoView';
import TodoForm from './TodoForm';

@connect(state => ({todos: state.todos}))
export default class TodoList extends React.Component {
    static propTypes = {
        todos:    PropTypes.any.isRequired,
        dispatch: PropTypes.func.isRequired
    }

    static needs = [
        TodoActions.getTodos
    ]

    render() {
        const { todos, dispatch } = this.props;

        return (
            <div id="todo-list">
                <TodoView todos={todos}
                    {...bindActionCreators(TodoActions, dispatch)} />
                <TodoForm
                    {...bindActionCreators(TodoActions, dispatch)} />
            </div>
        );
    }
}

