'use strict';

import React from 'react';
import { Router, Route, Link, IndexRoute  } from 'react-router';

import * as TodoComponents  from './components';

export default (
    <Route name="app" component={TodoComponents.App} path="/">
        <IndexRoute component={TodoComponents.Home} />
        <Route path="hello/:who" component={TodoComponents.Hello} />
        <Route path="todo" component={TodoComponents.TodoApp} />
        <Route path="tree" component={TodoComponents.TreeApp} />
    </Route>
);