'use strict';

import { List, fromJS } from 'immutable';

import * as TodoActions from '../actions/TodoActions';

const defaultState = new List(
    [
        'test_0',
        'Test_1'
    ]
);

class TodoStorage {
    // TODO сделать как promise
    key = 'todo';

    constructor() {
        this.storage = localStorage;
        this.todos = this.loadTodos();
    }

    loadTodos() {
        return fromJS(JSON.parse(this.storage[this.key] || "[]"));
    }

    getTodos() {
        return this.todos;
    }

    sync(todos) {
        this.todos = todos || this.todos;
        this.storage[this.key] = JSON.stringify(todos)
    }

    createTodo(text) {
        this.sync(this.todos.concat(text));
        return this;

    }

    editTodo(id, text) {
        this.sync(
            this.set(action.id, action.text)
        );
        return this;
    }

    deleteTodo(id) {
        this.sync(this.todos.delete(id));
        return this;
    }

}

//const defaultState = new TodoStorage();

export default function todoReducer(state = defaultState, action) {
    switch (action.type) {
        case TodoActions.CREATE_TODO:
            return state.concat(action.text);
        case TodoActions.EDIT_TODO:
            return state.set(action.id, action.text);
        case TodoActions.DELETE_TODO:
            return state.delete(action.id);
        default:
            return state;
    }
}
